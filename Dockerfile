FROM rclone/rclone

RUN wget https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2018-09.csv

RUN head -n 15 < /data/yellow_tripdata_2018-09.csv > /data/data_short.csv 
RUN cat /data/data_short.csv 

RUN apk add --update postgresql-client

# Configure pg password file.
COPY ./.pgpass ./.pgpass
RUN chmod 0600 ./.pgpass
ENV PGPASSFILE='/data/.pgpass'

# Note1: User, hostname & database are needed even with a .pgpass
# Note2: table with upper case characters are a pain in the a... !!!!! This is why it's in all lower case.
# Note3: Ce call est hyper lent dans sa forme actuelle pour des gros fichiers.
# Note4: Pour ce fichier il y a une erreur csv très loin dans le fichier :( -> ERROR:  invalid input syntax for type numeric: "VendorID" && CONTEXT:  COPY taxi_data, line 1, column VendorID: "VendorID"
RUN psql -U kears4 -h postgres-2.dev.valeria.science ul-val-prj-valeria-exp01 -c "\COPY public.taxi_data from '/data/data_short.csv' delimiter ',' csv;"